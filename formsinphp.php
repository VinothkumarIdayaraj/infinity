<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>PHP Forms</title>
	</head>
	
	<body>
		<?php
		echo $_SESSION["testname"];
		if(isset($_POST["formSubmit"])){
			echo "Username: " . $_REQUEST["username"];
			echo "<br>Email ID: " . $_POST["useremail"];
			echo "<br>Phone: " . $_POST["userphone"];
		}
		
		if(isset($_POST["loginSubmit"])){
			echo "Username: " . $_POST["username"];
			echo "<br>Password: " . $_POST["userpassword"];
		}
		?>
		
		<div>Form 1</div>
		<form name="userLogin" action="form.php" method="post">
			<table>
				<tr>
					<th>Username:</th>
					<td><input type="text" name="username" value="" required></td>
				</tr>
				
				<tr>
					<th>Password:</th>
					<td><input type="password" name="userpassword" value="" required></td>
				</tr>
				
				<tr>
					<td colspan=2>
						<input type="submit" name="loginSubmit" value="Login">
					</td>
				</tr>
			</table>
		</form>
		
		<div>Form 2</div>
		<form name="userDetails" action="form.php" method="post">
			<table>
				<tr>
					<th>Name:</th>
					<td><input type="text" name="username" value="" required></td>
				</tr>
				
				<tr>
					<th>Email:</th>
					<td><input type="email" name="useremail" value="" required></td>
				</tr>
				
				<tr>
					<th>Phone:</th>
					<td><input type="text" name="userphone" value="" required></td>
				</tr>
				
				<tr>
					<td colspan=2>
						<input type="button" name="formCancel" value="Cancel">
						<input type="submit" name="formSubmit" value="Submit">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>